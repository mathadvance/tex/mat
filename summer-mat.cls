\ProvidesClass{summer-mat}

\LoadClass{article}

\RequirePackage{xcolor} % has to be before tikz

\RequirePackage{amsmath,amssymb}
\RequirePackage{asymptote,tikz,tikz-3dplot}
	\begin{asydef}
		usepackage("newpxtext, newpxmath");
	\end{asydef}

\pagenumbering{gobble}

% Set dimensions via geometry.
\RequirePackage[letterpaper,top=2cm,bottom=4cm,left=3cm,right=3cm,includeheadfoot]{geometry}

%%%%%%%%%
%LENGTHS%
%%%%%%%%%

\setlength{\parindent}{0pt}

%%%%%%%
%FONTS%
%%%%%%%

\DeclareRobustCommand{\alegreyafont}{\fontfamily{Alegreya-LF}\selectfont} % defines alegreya
\DeclareRobustCommand{\parttitlefont}{\alegreyafont}

\RequirePackage{newpxtext,newpxmath}

%%%%%%%%%%%%%%%%%%
%PROB/SOL DISPLAY%
%%%%%%%%%%%%%%%%%%

\newenvironment{content}{\vskip -2mm\setlength{\parskip}{1ex plus 0.5ex minus 0.2ex}}{}

\newcommand{\ansbold}[1]{
	\ifmmode
		{\sectioncolor\mathbf{#1}}
	\else
		\ClassError{ma}{The \protect\ansbold command must be used in math mode.}
	\fi
}

%%%%%%%%
%COLORS%
%%%%%%%%

\definecolor{darkpurple}{HTML}{2d005a}

\newcommand{\sectioncolor}{\color{darkpurple}}

%%%%%%%%%
%HEADERS%
%%%%%%%%%

\RequirePackage{graphicx}

\RequirePackage{fancyhdr}
\pagestyle{fancyplain}
\renewcommand{\headrulewidth}{0pt}
\fancyhead{}

\fancyfoot[L]{
	\raisebox{-60pt}[0pt][0pt]{
		\includegraphics[width=2cm]{mountain.png}
	}
	\raisebox{-56pt}[0pt][0pt]{
		\includegraphics[width=2cm]{typography.png}
	}
}

\renewcommand\maketitle{
	\thispagestyle{empty}
	\begin{center}
		{\Huge\parttitlefont\@title}
		\vskip\baselineskip
		\includegraphics[width=4cm]{mountain.png}
		\vskip\baselineskip
	\end{center}
}

\renewcommand{\part}[1]{
	\newpage
	\begin{center}
		{\Huge\parttitlefont#1}
		\vskip\baselineskip
	\end{center}
}

\renewcommand{\section}[1]{
	\vskip 6mm
		{\LARGE\sectioncolor\bfseries\sffamily #1}
	\vskip 2mm
}

\renewcommand{\subsection}[1]{
	\vskip 6mm
		{\large\sectioncolor\bfseries\sffamily #1}
	\vskip 2mm
}

%%%%%%%
%ITEMS%
%%%%%%%

\RequirePackage{pifont}
\renewcommand{\labelitemi}{\ding{70}}

\renewcommand\emph[1]{\textbf{#1}}
