# Installation Instructions

To clone this repository inside your TEXMF, run

```
git clone https://gitlab.com/mathadvance/tex/mat ~/texmf/tex/latex/mat
```

You may need to create the folder `~/texmf/tex/latex`; to do this, run `mkdir -p ~/texmf/tex/latex`.

The MAT contest class is meant to be used with mapm, period. Install mapm and set it up by following the instructions at [mapm.mathadvance.org/](https://mapm.mathadvance.org/). Then symlink the `mapm` folder over to `$CONFIG/mapm/templates/mat`.

You can find `$CONFIG` in the [documentation of the dirs crate](https://docs.rs/dirs/latest/dirs/fn.config_dir.html); on Linux it is typically `~/.config`. To do this, run

```
ln -s ~/texmf/tex/latex/mat/mapm ~/.config/mapm/templates/mat
```

The document class can still compile without mapm; however, all the document class provides is visuals, with none of the default formatting, so just don't.
